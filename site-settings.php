<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.digid',
    'namespace'   => 'ApiNovumDigid',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'digid.demo.novum.nu',
    'dev_domain'  => 'digid.demo.novum.nuidev.nl',
    'test_domain' => 'digid.test.demo.novum.nu',
];
